package es.osi.stereovision;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.GrayFilter;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args)
    {
        process("L", new File("data/pictures/DSCN1149.JPG"));
        process("R", new File("data/pictures/DSCN1150.JPG"));

    }
    
    /** Entry point */
    private static void process(String id, File file)
    {
        try {
            
            BufferedImage img = ImageIO.read(file);
            debug(img);
            
            BufferedImage image = null;
            

            {
                image = new BufferedImage(
                        img.getWidth(), img.getHeight(),  
                        BufferedImage.TYPE_USHORT_GRAY);  
                Graphics2D g = (Graphics2D) image.getGraphics();  
                g.drawImage(img, 0, 0, null);  
                g.dispose();
            }


            /*
            {
                ImageFilter filter = new GrayFilter(true, 20);  
                ImageProducer producer = new FilteredImageSource(img.getSource(), filter);  
                Image gray = Toolkit.getDefaultToolkit().createImage(producer);
                
                image = new BufferedImage(
                        gray.getWidth(null), gray.getHeight(null), 
                        BufferedImage.TYPE_INT_ARGB);

                image = new BufferedImage(
                        gray.getWidth(null), gray.getHeight(null), 
                        BufferedImage.TYPE_BYTE_GRAY);
                
                image = new BufferedImage(
                        gray.getWidth(null), gray.getHeight(null), 
                        BufferedImage.TYPE_USHORT_GRAY);
                
                // Draw the image on to the buffered image
                Graphics2D bGr = image.createGraphics();
                bGr.drawImage(gray, 0, 0, null);
                bGr.dispose();
            }
            */
                
            // Reduce image size
            int width = image.getWidth();
            int height = image.getHeight();
            
            double scale = 1./4;
            int dwidth = (int) (scale * width);
            int dheight = (int) (scale * height);
            
            BufferedImage scaled = new BufferedImage(dwidth, dheight, image.getType());
            Graphics2D g = scaled.createGraphics();
            AffineTransform at = AffineTransform.getScaleInstance(scale, scale);
            g.drawRenderedImage(image, at);
            g.dispose();

            // Show it
            showPicture(id + " original", scaled);
            debug(scaled);
            
            // Laplace
            BufferedImage filtered = filterLaplace(scaled);

            // show result
            showPicture(id + " laplace", filtered);
            debug(filtered);
            
            
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    private static void showPicture(String title, final BufferedImage image) {
        
        // Reduce image size
        int width = image.getWidth();
        int height = image.getHeight();
        
        JFrame frame = buildFrame(title, width, height);

        JPanel pane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 0, 0, null);
            }
        };


        frame.add(pane);
    }
    
    private static BufferedImage filterLaplace(BufferedImage image) {
        
        int width = image.getWidth();
        int height = image.getHeight();
        
//        int[] mean = averageColor(image);
//        System.out.println("Mean: " + Arrays.toString(mean));

        Raster orig = image.getRaster();
        BufferedImage filtered = new BufferedImage(width, height, image.getType());
        
        int[][][] buffer = new int[width][height][];
        for (int x = 1; x < (width - 1); ++x) {
            for (int y = 1 ; y < (height - 1); ++ y) {
                
                int[] array = new int[4];
                int value = value4(x, y, orig);
                array[0] = value;
              
                buffer[x][y] = array;
            }
        }
        
        // Rescale aux buffer
        rescale(buffer);
        
        // Copy aux buffer into the image
        int[] array;
        WritableRaster raster = filtered.getRaster();
        for (int x = 1; x < (width - 1); ++x) {
            for (int y = 1 ; y < (height - 1); ++ y) {
                array = buffer[x][y];
                raster.setPixel(x, y, array);
            }
        }
        
        return filtered;
    }
    
    /** Take into account the borders: They are set to zero by the filter */
    private static void rescale(int[][][] buffer) {
        
        final long RANGE = (long) Math.pow(2, 16); // TYPE_USHORT_GRAY
        //final long RANGE = (long) Math.pow(2, 8); // TYPE_BYTE_GRAY
        
        int width = buffer.length;
        int height = buffer[0].length;
        
        int[][] colorRanges = colorRanges(buffer);
        
        System.out.println("Min color ranges: " + Arrays.toString(colorRanges[0]));
        System.out.println("Max color ranges: " + Arrays.toString(colorRanges[1]));


        int[] array;
        for (int x = 1; x < (width - 1); ++x) {
            for (int y = 1 ; y < (height - 1); ++ y) {
                array = buffer[x][y];
                for (int i=0; i<array.length; ++i) {
                    int min = colorRanges[0][i];
                    int max = colorRanges[1][i];
                    if (min != max) {
                        array[i] = (int) ((RANGE * (array[i] - min)) / (max - min)) ;
                    } else {
                        array[i] = 0;
                    }
                }
            }
        }
    }
    
    private static int value4(int x, int y, Raster r) {

        int value = 4 * r.getSample(x, y, 0);
        
        value = value - r.getSample(x, y-1, 0);
        
        value = value - r.getSample(x-1, y, 0);
        value = value - r.getSample(x+1, y, 0);
        
        value = value - r.getSample(x, y+1, 0);

        return value;
    }
    
    private static int value8(int x, int y, Raster r) {

        int value = 8 * r.getSample(x, y, 0);
        
        value = value - r.getSample(x-1, y-1, 0);
        value = value - r.getSample(x  , y-1, 0);
        value = value - r.getSample(x+1, y-1, 0);
        
        value = value - r.getSample(x-1, y, 0);
        value = value - r.getSample(x+1, y, 0);
        
        value = value - r.getSample(x-1, y+1, 0);
        value = value - r.getSample(x  , y+1, 0);
        value = value - r.getSample(x+1, y+1, 0);

        return value;
    }

    private static int[] averageColor(BufferedImage bi) {
        
        
        WritableRaster raster = bi.getRaster();
        long[] acc = new long[4];

        int[] data = new int[4];
        for (int x = 0; x < bi.getWidth(); ++x) {
            for (int y = 0; y < bi.getHeight(); ++y) {
                raster.getPixel(x, y, data);
                for (int i = 0; i < acc.length; ++i) acc[i] += data[i];
            }
        }
        
        // Mean
        int[] mean = new int[4];
        int elements = bi.getHeight() * bi.getWidth();
        for (int i = 0; i < mean.length; ++i) mean[i] = (int) (acc[i] / elements);
        
        return mean;
    }
    
    private static int[][] colorRanges(BufferedImage bi) {
        
        WritableRaster raster = bi.getRaster();
        
        int[] min = new int[4];
        for (int i = 0; i < min.length; ++i) min[i] = Integer.MAX_VALUE;

        int[] max = new int[4];
        for (int i = 0; i < max.length; ++i) max[i] = Integer.MIN_VALUE;

        int[] data = new int[4];
        for (int x = 0; x < bi.getWidth(); ++x) {
            for (int y = 0; y < bi.getHeight(); ++y) {
                raster.getPixel(x, y, data);
                // Minimums
                for (int i = 0; i < min.length; ++i) if (data[i] < min[i]) min[i] = data[i];
                // Maximums
                for (int i = 0; i < max.length; ++i) if (data[i] > max[i]) max[i] = data[i];
            }
        }
        
        return new int[][] { min, max };
    }
    
    /** Take into account the borders: They are set to zero by the filter */
    private static int[][] colorRanges(int[][][] buffer) {
        
        int[] min = new int[4];
        for (int i = 0; i < min.length; ++i) min[i] = Integer.MAX_VALUE;

        int[] max = new int[4];
        for (int i = 0; i < max.length; ++i) max[i] = Integer.MIN_VALUE;

        int[] data;
        for (int x = 1; x < buffer.length - 1; ++x) {
            for (int y = 1; y < buffer[0].length -1; ++y) {
                data = buffer[x][y];
                // Minimums
                for (int i = 0; i < min.length; ++i) if (data[i] < min[i]) min[i] = data[i];
                // Maximums
                for (int i = 0; i < max.length; ++i) if (data[i] > max[i]) max[i] = data[i];
            }
        }
        
        return new int[][] { min, max };
    }
    
    private static void debug(BufferedImage image) {
        
        System.out.println("Image: " + image);
        
        WritableRaster raster = image.getRaster();
        int[] data = new int[4];
        for (int x = 0; x < 4; ++x) {
            for (int y = 0; y < 4; ++y) {
                raster.getPixel(x, y, data);
                System.out.println(Arrays.toString(data));
            }
        }
    }
    
    private static JFrame buildFrame(String title, int width, int height) {
        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(width, height);
        frame.setVisible(true);
        return frame;
    }

}
